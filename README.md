# aacc-udoit-alpine

This project aims to provide [Alpine](https://hub.docker.com/_/alpine/)
based containers for the [UDOIT](https://github.com/ucfopen/UDOIT)
application for use in Kubernetes clusters.

## Versions

Each version of aacc-udoit-alpine is built against a different version of
Alpine and/or UDOIT. While aacc-udoit-alpine does include some components
which may necessitate patch version bumps, major/minor version changes will
be determined by changes to Alpine and/or UDOIT alone.

| Chart Version | Image Version | Alpine Version | UDOIT Version |
|---------------|---------------|----------------|---------------|
| 0.3.0         | 0.3.0         | 3.18           | 3.3.2         |
| 0.2.2         | 0.2.2         | 3.17           | 3.3.1         |

## Environment Variables

| Variable Name | Description                                  |
|---------------|----------------------------------------------|
| APP_ENV       | Used by symfony/framework-bundle.            |
| APP_LMS       | The Learning Management System (LMS) flavor. |
| BASE_URL      | The base URL for link generation.            |
| DATABASE_URL  | Used by doctrine/doctrine-bundle.            |
