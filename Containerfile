FROM alpine:3.18

# The current symfony-cli release as of 2023-06-29.
ARG SYMFONY_CLI_VERSION=5.5.6
# For supporting multi-platform building.
ARG TARGETPLATFORM
# The current UDOIT release as of 2022-04-20.
ARG UDOIT_VERSION=3.3.2

# Perform build time tasks.
RUN \
    set -eux ; \
    # Install nginx, php-fpm8, and other dependencies. \
    apk add --no-cache "composer" "nginx" "npm" "nodejs-current" \
        "php81" "php81-ctype" "php81-dom" "php81-fpm" "php81-gd" \
        "php81-pdo_mysql" "php81-pdo_pgsql" "php81-session" \
        "php81-simplexml" "php81-sodium" "php81-tokenizer" \
        "php81-xml" "supervisor" "yarn" ; \
    # Install symfony-cli from GitHub. \
    cd "/usr/local/bin" ; \
    case "$TARGETPLATFORM" in \
    "linux/amd64") _ARCH=amd64 ;; \
    "linux/arm64"|"linux/arm64/v8") _ARCH=arm64 ;; \
    *) \
        printf 'error: unsupported platform: %s\n' \
            "${TARGETPLATFORM:-undefined}" >&2 ; \
        exit 1 \
        ;; \
    esac ; \
    wget -O /tmp/symfony-cli.tar.gz "https://github.com/symfony-cli\
/symfony-cli/releases/download/v$SYMFONY_CLI_VERSION\
/symfony-cli_linux_$_ARCH.tar.gz" ; \
    tar -xpf /tmp/symfony-cli.tar.gz symfony ; \
    rm /tmp/symfony-cli.tar.gz ; \
    # Build time tasks for nginx. \
    chown nginx:nginx "/var/log/php81" ; \
    # Install UDOIT from GitHub. \
    cd "/opt" ; \
    wget -O "/tmp/udoit.tar.gz" "https://github.com/ucfopen/UDOIT/archive/\
$UDOIT_VERSION.tar.gz" ; \
    tar -xpf "/tmp/udoit.tar.gz" ; \
    rm "/tmp/udoit.tar.gz" ; \
    mv "UDOIT-$UDOIT_VERSION" "udoit" ; \
    cd "/opt/udoit" ; \
    # Prepare the UDOIT application. \
    composer install --no-dev --no-interaction --no-progress \
        --optimize-autoloader ; \
    apk add --no-cache "g++" "make" "python3" ; \
    yarn install ; \
    apk del --no-cache "g++" "make" "python3" ; \
    NODE_OPTIONS="--openssl-legacy-provider" yarn run "encore" dev ; \
    yarn cache clean ; \
    cp .env.example .env ; \
    chown -R "nginx:nginx" "/opt/udoit"
COPY --chown=100:101 etc/nginx/http.d/default.conf \
                     /etc/nginx/http.d/
COPY --chown=100:101 etc/nginx/nginx.conf /etc/nginx/
COPY --chown=100:101 "etc/php81/conf.d/50-variables_order.ini" \
                     "etc/php81/conf.d/51-memory_limit.ini" \
                     "/etc/php81/conf.d/"
COPY --chown=100:101 "etc/php81/php-fpm.conf" \
                     "/etc/php81/php-fpm.conf"
COPY --chown=100:101 "etc/php81/php-fpm.d/www.conf" \
                     "/etc/php81/php-fpm.d/"
COPY --chown=100:101 etc/supervisord.conf /etc/
COPY --chown=100:101 entrypoint.sh /
USER 100
EXPOSE 8080
WORKDIR "/opt/udoit"
ENTRYPOINT ["/entrypoint.sh"]
